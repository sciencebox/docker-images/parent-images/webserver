#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#
#       |S|c|i|e|n|c|e| |B|o|x|        #
#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#

# Docker file for base image

# Build and push to Docker registry with:
#   export RELEASE_VERSION=":v0"
#   docker build -t gitlab-registry.cern.ch/sciencebox/docker-images/parent-images/webserver${RELEASE_VERSION} -f webserver.Dockerfile .
#   docker login gitlab-registry.cern.ch
#   docker push gitlab-registry.cern.ch/sciencebox/docker-images/parent-images/webserver${RELEASE_VERSION}


FROM gitlab-registry.cern.ch/sciencebox/docker-images/parent-images/base:v0

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Supervisord ----- #
RUN yum -y install \
      supervisor && \
    yum clean all && \
    rm -rf /var/cache/yum
ADD ./supervisord.d/supervisord.conf /etc/supervisord.conf

# ----- Httpd ----- #
RUN yum -y install \
      httpd \
      mod_ssl && \
    yum clean all && \
    rm -rf /var/cache/yum

# ----- Shibboleth ----- #
RUN yum -y install \
      shibboleth \
      opensaml-schemas && \
    yum clean all && \
    rm -rf /var/cache/yum

# ----- SSSD ----- #
RUN yum -y install \
      sssd && \
    yum clean all && \
    rm -rf /var/cache/yum

# ----- Logrotate ---- #
RUN yum -y install \
      logrotate && \
    yum clean all && \
    rm -rf /var/cache/yum
ADD ./logrotate.d/logrotate.conf /etc/logrotate.conf
RUN chmod 644 /etc/logrotate.conf
ADD ./logrotate.d/logrotate.cronjob /etc/cron.hourly/logrotate
RUN chmod +x /etc/cron.hourly/logrotate

# ----- Add logrotate files for installed daemons ----- #
ADD ./logrotate_jobs.d/httpd /etc/logrotate.d/httpd
ADD ./logrotate_jobs.d/shibd /etc/logrotate.d/shibd
ADD ./logrotate_jobs.d/sssd /etc/logrotate.d/sssd

# ----- Add supervisord configuration files for installed daemons ----- #
ADD ./supervisord.d/kill_supervisor.py /etc/supervisord.d/kill_supervisor.py
ADD ./supervisord.d/kill_supervisor.ini /etc/supervisord.d/kill_supervisor.ini
ADD ./supervisord.d/httpd.ini /etc/supervisord.d/httpd.noload
ADD ./supervisord.d/shibd.ini /etc/supervisord.d/shibd.noload
ADD ./supervisord.d/sssd.ini /etc/supervisord.d/sssd.noload
ADD ./supervisord.d/crond.ini /etc/supervisord.d/crond.noload
